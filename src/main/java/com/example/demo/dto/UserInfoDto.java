package com.example.demo.dto;

public class UserInfoDto {
    private int id;
    private String name;
    private String email;

    public UserInfoDto(int id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    public UserInfoDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
