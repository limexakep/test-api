package com.example.demo.dto;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(name = "Пользователь", description = "Сущность для добавления пользователя")
public class UserDto {
    @Schema(name = "name", description = "Фамилия пользователя")
    private String name;
    @Schema(name = "email", description = "Email пользователя")
    private String email;
    @Schema(name = "password", description = "Пароль пользователя")
    private String password;

    public UserDto(String name, String email, String password) {
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public UserDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
