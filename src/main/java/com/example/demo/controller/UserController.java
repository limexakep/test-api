package com.example.demo.controller;

import com.example.demo.dto.UserDto;
import com.example.demo.dto.UserInfoDto;
import com.example.demo.model.User;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;

@RestController
@RequestMapping("/user")
@Tag(name = "Пользователь", description = "Контроллер по управлению пользователями")
public class UserController {
    private static final String MOEX_URL = "https://iss.moex.com/iss/securities/SBER.json";

    public static List<User> users = new ArrayList<>();

    {
        users.add(new User(User.idCount++, "Ivan", "mail@mail.ru", "qwerty"));
        users.add(new User(User.idCount++, "Fyodor", "mail2@mail.ru", "12345"));
    }

    @GetMapping("/")
    public List<UserInfoDto> getUsers() {
        List<UserInfoDto> dtoUsers = new ArrayList<>();

        users.forEach(user -> dtoUsers.add(new UserInfoDto(user.getId(), user.getName(), user.getEmail())));

        return dtoUsers;
    }

    @GetMapping("/moex")
    public String getMoexData() {
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<String> response = restTemplate.getForEntity(MOEX_URL, String.class);

        return response.getBody();
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserInfoDto> getUserById(@PathVariable int id) {
        Optional<User> user = users.stream()
            .filter(u -> u.getId() == id)
            .findAny();

        if (user.isEmpty()) {
            return ResponseEntity
                .status(HttpStatus.I_AM_A_TEAPOT)
                .body(null);
        } else {
            return ResponseEntity
                .status(HttpStatus.OK)
                .body(new UserInfoDto(user.get().getId(), user.get().getName(), user.get().getEmail()));
        }
    }

    @PostMapping("/")
    public int addUser(@RequestBody UserDto userDto) {
        User user = new User(User.idCount++, userDto.getName(), userDto.getEmail(), userDto.getPassword());
        users.add(user);

        return user.getId();
    }

    @PutMapping("/{id}")
    public int editUser(@PathVariable int id, @RequestBody UserDto userDto) {
        User user = users.stream()
            .filter(u -> u.getId() == id)
            .findAny()
            .orElse(null);

        if (user == null) {
            throw new ResponseStatusException(
                HttpStatus.NOT_FOUND, "Пользователь не найден!"
            );
        } else {
            user.setEmail(userDto.getEmail());
            user.setName(userDto.getName());
            user.setPassword(userDto.getPassword());

            return user.getId();
        }
    }

    @DeleteMapping("/{id}")
    public boolean editUser(@PathVariable int id) {
        User user = users.stream()
            .filter(u -> u.getId() == id)
            .findAny()
            .orElse(null);

        if (user == null) {
            throw new ResponseStatusException(
                HttpStatus.NOT_FOUND, "Пользователь не найден!"
            );
        } else {
            users.remove(user);

            return true;
        }
    }

    @GetMapping("/error/{id}")
    public ResponseEntity<UserInfoDto> getUserByIdWithError(@PathVariable int id) {
        return ResponseEntity.notFound().build();
    }

}
